package com.json;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ConvertJSONToJava 
{
	public static void main(String[] args) 
	{
	    // convert JSON array to list of doctors
	    File currentDirectory = new File(new File(".").getAbsolutePath());

		final String uri = currentDirectory.getAbsolutePath()+"\\src\\com\\json\\DoctorsList.json";

		try 
		{
		    // create object mapper instance
		    ObjectMapper mapper = new ObjectMapper();
		    
		    List<Doctor> books = Arrays.asList(mapper.readValue(Paths.get(uri).toFile(), Doctor[].class));

		    // print doctors
		    books.forEach(System.out::println);

		} 
		catch (InvalidPathException e) {
			System.out.println("Could not find the json file in the mentioned path ["+uri+"]");
		}
		catch (JsonParseException e) {
			System.out.println("Could not parse the json file in the mentioned path ["+uri+"]");
		}
		catch (Exception ex) {
		    System.out.println("Generic exception caught while processing : "+ex.getMessage());
		}

	}

}
