package com.my;

import java.util.HashMap;
import java.util.Map;

public class SimpleOperations {

	public int countLength(String str)
	{
		int count = 0;
		
		if(str!=null)
		count = str.length();
		
		return count;
	}
	
	public int sumOf(int x, int y) {
		return x+y;
	}
	
	
	public int multiply(int x, int y) {
		return x*y;
	}
	
	public String getPropValue(String key){
        Map<String, String> appProps = new HashMap<String, String>();
        appProps.put("key1", "value 1");
        appProps.put("key2", "value 2");
        appProps.put("key3", "value 3");
        return appProps.get(key);
    }
}
