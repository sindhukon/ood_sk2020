package com.my;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TreeNode {
    boolean word;
    HashMap<Character, TreeNode> children = new HashMap<>();

    public TreeNode get(char value) {
        // Get anode from the HashMap.
        return children.getOrDefault(value, null);
    }

    public TreeNode add(char value, TreeNode node) {
        // Add a node if one does not yet exists.
        // ... Return the matching node.
        children.putIfAbsent(value, node);
        return get(value);
    }

    public String toString(){
        String output = "";
        Iterator chItr = children.entrySet().iterator();
        while(chItr.hasNext()){
            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)chItr.next();
            char key = ((char)entry.getKey());
            boolean value = ((TreeNode)entry.getValue()).word;
            output = output+"Key - "+key+" : Value -  "+value +"\n";
        }

        return  output;

    }
}